//
//  PublicIntegrationTestCase.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 25/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import XCTest
@testable import MagicText

class PublicIntegrationTests: XCTestCase {
    
    private let string = "We only test the label. All other components are tested individually in other tests (especially MagicTextRangeGenerator) so we only te_st the effects on string and attributedText on the label"
    private let target = ["test"]
    private let targetRange: [NSRange] = [NSRange(location: 8, length: 4), NSRange(location: 49, length: 4), NSRange(location: 78, length: 4)]
    private let font = UIFont.systemFont(ofSize: 10)
    
    func testLabelBold() {
        let label = createLabel()
        
        label.highlight(types: [.bold], occurancesOf: target, location: .anywhere)
        
        let expectedText = createAttributedString(changeTypes: [.bold])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    func testLabelItalic() {
        let label = createLabel()
        
        label.highlight(types: [.italic], occurancesOf: target, location: .anywhere)
        
        let expectedText = createAttributedString(changeTypes: [.italic])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    func testLabelUnderline() {
        let label = createLabel()
        
        label.highlight(types: [.underline(style: .byWord, color: UIColor.yellow)], occurancesOf: target)
        
        let expectedText = createAttributedString(changeTypes: [.underline(style: .byWord, color: UIColor.yellow)])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    func testLabelSizeChange() {
        let label = createLabel()
        
        label.highlight(types: [.size(increment: 10)], occurancesOf: target, location: .anywhere, caseSensitive: false)
        
        let expectedText = createAttributedString(changeTypes: [.size(increment: 10)])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    func testLabelBoldUnderline() {
        let label = createLabel()
        
        label.highlight(types: [.bold, .underline(style: .double, color: UIColor.blue)], occurancesOf: target)
        
        let expectedText = createAttributedString(changeTypes: [.bold, .underline(style: .double, color: UIColor.blue)])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    func testLabelItalicUnderline() {
        let label = createLabel()
        
        label.highlight(types: [.italic, .underline(style: .double, color: UIColor.blue)], occurancesOf: target)
        
        let expectedText = createAttributedString(changeTypes: [.italic, .underline(style: .double, color: UIColor.blue)])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    func testLabelSizeUnderline() {
        let label = createLabel()
        
        label.highlight(types: [.size(increment: 1), .underline(style: .double, color: UIColor.blue)], occurancesOf: target)
        
        let expectedText = createAttributedString(changeTypes: [.size(increment: 1), .underline(style: .double, color: UIColor.blue)])
        XCTAssertEqual(label.attributedText!, expectedText)
    }
    
    private func createLabel() -> UILabel {
        let label = UILabel()
        label.font = font
        label.text = string
        return label
    }
    
    private func createAttributedString(changeTypes: [MagicText.FontChangeType]) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: string)
        
        // Because size, bold and italic all access the key .font, combining them will result in attributes being overritten and false results. Please DON'T MIX in this test attributes that share the same key
        changeTypes.forEach { changeType in
            targetRange.forEach { range in
                switch changeType {
                case .size(let increment):
                    mutableString.addAttribute(.font, value: font.applyAttributes([], sizeChange: increment), range: range)
                case .bold:
                    mutableString.addAttribute(.font, value: font.applyAttributes([.traitBold]), range: range)
                case .italic:
                    mutableString.addAttribute(.font, value: font.applyAttributes([.traitItalic]), range: range)
                case .underline(let style, let color):
                    mutableString.addAttribute(.underlineStyle, value: style.rawValue, range: range)
                    if let color = color {
                        mutableString.addAttribute(.underlineColor, value: color, range: range)
                    }
                }
            }
        }
        
        return mutableString
    }
}
