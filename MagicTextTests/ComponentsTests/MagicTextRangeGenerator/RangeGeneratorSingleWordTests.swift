//
//  RangeGeneratorSingleWordTests.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 17/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//
import XCTest
@testable import MagicText

class RangeGeneratorSingleWordTests: XCTestCase {
    
    private let string = "This test has only 1 correct result"
    
    // MARK: - Anywhere
    func testAnywhere() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["orRec"], occuringIn: .anywhere, isCaseSensitive: false)
        let range2 = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["orrec"], occuringIn: .anywhere, isCaseSensitive: false)
        
        XCTAssertEqual(range, [NSRange(location: 22, length: 5)])
        XCTAssertEqual(range, range2)
    }
    
    func testAnywhereCaseSensitivePass() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["orrec"], occuringIn: .anywhere, isCaseSensitive: true)
        
        XCTAssertEqual(range, [NSRange(location: 22, length: 5)])
    }
    
    func testAnywhereCaseSensitiveFail() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["orRec"], occuringIn: .anywhere, isCaseSensitive: true)
        
        XCTAssertEqual(range, [])
    }
    
    // MARK: - Start
    func testStart() {
        let rangeEnd = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["nly"], occuringIn: .end, isCaseSensitive: false)
        let rangeStart = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["nly"], occuringIn: .start, isCaseSensitive: false)
        
        XCTAssertEqual(rangeEnd, [NSRange(location: 15, length: 3)])
        XCTAssertEqual(rangeStart, [])
    }
    
    func testStartCaseSensitivePass() {
        let rangeEnd = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["nly"], occuringIn: .end, isCaseSensitive: true)
        let rangeStart = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["nly"], occuringIn: .start, isCaseSensitive: true)
        
        XCTAssertEqual(rangeEnd, [NSRange(location: 15, length: 3)])
        XCTAssertEqual(rangeStart, [])
    }
    
    func testStartCaseSensitiveFail() {
        let rangeEnd = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["Nly"], occuringIn: .end, isCaseSensitive: true)
        let rangeStart = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["Nly"], occuringIn: .start, isCaseSensitive: true)
        
        XCTAssertEqual(rangeEnd, [])
        XCTAssertEqual(rangeStart, [])
    }
    
    // MARK: - End
    func testEnd() {
        let rangeStart = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["onl"], occuringIn: .start, isCaseSensitive: false)
        let rangeEnd = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["onl"], occuringIn: .end, isCaseSensitive: false)
        
        XCTAssertEqual(rangeStart, [NSRange(location: 14, length: 3)])
        XCTAssertEqual(rangeEnd, [])
    }
    
    func testEndCaseSensitivePass() {
        let rangeStart = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["onl"], occuringIn: .start, isCaseSensitive: true)
        let rangeEnd = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["onl"], occuringIn: .end, isCaseSensitive: true)
        
        XCTAssertEqual(rangeStart, [NSRange(location: 14, length: 3)])
        XCTAssertEqual(rangeEnd, [])
    }
    
    func testEndCaseSensitiveFail() {
        let rangeStart = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["onL"], occuringIn: .start, isCaseSensitive: true)
        let rangeEnd = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: ["onL"], occuringIn: .end, isCaseSensitive: true)
        
        XCTAssertEqual(rangeStart, [])
        XCTAssertEqual(rangeEnd, [])
    }
    
}
