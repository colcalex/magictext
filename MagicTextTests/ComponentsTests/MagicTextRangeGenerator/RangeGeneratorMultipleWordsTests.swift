//
//  RangeGeneratorMultipleWordsTests.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 17/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//
import XCTest
@testable import MagicText

// This test case is a little extreme so if others are failing, debug them first
class RangeGeneratorMultipleWordTests: XCTestCase {
    
    private let string = "Multiple matches are expected from this teszt. Even a single cHeck should expect multiple results and muLtiPleZ and matmul"
    private let targets = ["z", "multiple", "mul", "le", "ch", "sul"]
    
    // MARK: - Anywhere
    func testAnywhere() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: targets, occuringIn: .anywhere, isCaseSensitive: false)
        
        let expectedRanges = [
            // z
            NSRange(location: 43, length: 1), NSRange(location: 110, length: 1),
            // multiple
            NSRange(location: 0, length: 8), NSRange(location: 81, length: 8), NSRange(location: 102, length: 8),
            // mul
            NSRange(location: 0, length: 3), NSRange(location: 81, length: 3), NSRange(location: 102, length: 3), NSRange(location: 119, length: 3),
            // le
            NSRange(location: 6, length: 2), NSRange(location: 58, length: 2), NSRange(location: 87, length: 2), NSRange(location: 108, length: 2),
            // ch
            NSRange(location: 12, length: 2), NSRange(location: 61, length: 2),
            // sul
            NSRange(location: 92, length: 3)]
        XCTAssertEqual(range, expectedRanges)
    }
    
    func testAnywhereCaseSensitive() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: targets, occuringIn: .anywhere, isCaseSensitive: true)
        
        let expectedRanges = [
            // z
            NSRange(location: 43, length: 1),
            // multiple
            NSRange(location: 81, length: 8),
            // mul
            NSRange(location: 81, length: 3), NSRange(location: 119, length: 3),
            // le
            NSRange(location: 6, length: 2), NSRange(location: 58, length: 2), NSRange(location: 87, length: 2), NSRange(location: 108, length: 2),
            // ch
            NSRange(location: 12, length: 2),
            // sul
            NSRange(location: 92, length: 3)]
        XCTAssertEqual(range, expectedRanges)
    }
    
    // MARK: - Start
    func testStart() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: targets, occuringIn: .start, isCaseSensitive: false)
        
        let expectedRanges = [
            // multiple
            NSRange(location: 0, length: 8), NSRange(location: 81, length: 8), NSRange(location: 102, length: 8),
            // mul
            NSRange(location: 0, length: 3), NSRange(location: 81, length: 3), NSRange(location: 102, length: 3),
            // ch
            NSRange(location: 61, length: 2)]
        XCTAssertEqual(range, expectedRanges)
    }
    
    func testStartCaseSensitive() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: targets, occuringIn: .start, isCaseSensitive: true)
        
        let expectedRanges = [
            // multiple
            NSRange(location: 81, length: 8),
            // mul
            NSRange(location: 81, length: 3)]
        XCTAssertEqual(range, expectedRanges)
    }
    
    // MARK: - End
    func testEnd() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: targets, occuringIn: .end, isCaseSensitive: false)
        
        let expectedRanges = [
            // z
            NSRange(location: 110, length: 1),
            // multiple
            NSRange(location: 0, length: 8), NSRange(location: 81, length: 8),
            // mul
            NSRange(location: 119, length: 3),
            // le
            NSRange(location: 6, length: 2), NSRange(location: 58, length: 2), NSRange(location: 87, length: 2)]
        XCTAssertEqual(range, expectedRanges)
    }
    
    func testEndCaseSensitive() {
        let range = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: targets, occuringIn: .end, isCaseSensitive: true)
        
        let expectedRanges = [
            // multiple
            NSRange(location: 81, length: 8),
            // mul
            NSRange(location: 119, length: 3),
            // le
            NSRange(location: 6, length: 2), NSRange(location: 58, length: 2), NSRange(location: 87, length: 2)]
        XCTAssertEqual(range, expectedRanges)
    }
    
}
