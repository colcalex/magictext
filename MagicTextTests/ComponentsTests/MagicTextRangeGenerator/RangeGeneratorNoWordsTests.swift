//
//  RangeGeneratorNoWords.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 17/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//
import XCTest
@testable import MagicText

class RangeGeneratorNoWordsTests: XCTestCase {
    
    private let string = "There should be no results to this test"
    
    func testAnywhere() {
        let ranges = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: [], occuringIn: .anywhere, isCaseSensitive: true)
        XCTAssertEqual(ranges, [])
    }
    
    func testStart() {
        let ranges = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: [], occuringIn: .start, isCaseSensitive: false)
        XCTAssertEqual(ranges, [])
    }
    
    func testEnd() {
        let ranges = MagicTextRangeGenerator(separators: " ").getRanges(in: string, of: [], occuringIn: .end, isCaseSensitive: true)
        XCTAssertEqual(ranges, [])
    }
    
}
