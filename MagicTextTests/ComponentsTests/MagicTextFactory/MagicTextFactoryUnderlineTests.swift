//
//  MagicTextFactoryUnderlineTests.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 25/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import XCTest
@testable import MagicText

class MagicTextFactoryUnderlineTests: XCTestCase {
    
    private let text = "Let's add some magic to this text memento somalic"
    private lazy var textFactory = MagicTextFactory(text: text, font: font)
    
    func testUnderlineAnywhere() {
        let configuration = createConfig(type: .underline(style: .single, color: nil), underline: "et's add so", locatedAt: .anywhere)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, ranges: [NSRange(location: 1, length: 11)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testUnderlineStart() {
        let configuration = createConfig(type: .underline(style: .single, color: nil), underline: "som", locatedAt: .start)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, ranges: [NSRange(location: 10, length: 3), NSRange(location: 42, length: 3)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testUnderlineEnd() {
        let configuration = createConfig(type: .underline(style: .single, color: nil), underline: "to", locatedAt: .end)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, ranges: [NSRange(location: 21, length: 2), NSRange(location: 39, length: 2)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testColorIsRecognised() {
        let configuration = createConfig(type: .underline(style: .single, color: nil), underline: "som", locatedAt: .start)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .double, ranges: [NSRange(location: 10, length: 3), NSRange(location: 42, length: 3)])
        XCTAssertNotEqual(mutableString, expectedString)
    }
    
    func testStyleIsRecognised() {
        let configuration = createConfig(type: .underline(style: .single, color: UIColor.blue), underline: "som", locatedAt: .start)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, color: UIColor.red, ranges: [NSRange(location: 10, length: 3), NSRange(location: 42, length: 3)])
        XCTAssertNotEqual(mutableString, expectedString)
    }
    
    func testUnderlineAnywhereWithColor() {
        let configuration = createConfig(type: .underline(style: .single, color: UIColor.red), underline: "et's add so", locatedAt: .anywhere)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, color: UIColor.red, ranges: [NSRange(location: 1, length: 11)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testUnderlineStartWithColor() {
        let configuration = createConfig(type: .underline(style: .single, color: UIColor.red), underline: "som", locatedAt: .start)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, color: UIColor.red, ranges: [NSRange(location: 10, length: 3), NSRange(location: 42, length: 3)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testUnderlineEndWithColor() {
        let configuration = createConfig(type: .underline(style: .single, color: UIColor.red), underline: "to", locatedAt: .end)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(style: .single, color: UIColor.red, ranges: [NSRange(location: 21, length: 2), NSRange(location: 39, length: 2)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    // MARK: - Private helpers
    private var font: UIFont {
        return UIFont.systemFont(ofSize: 0)
    }
    
    private func createConfig(type: MagicText.FontChangeType, underline: String, locatedAt: MagicText.LocationMatchType) -> MagicTextConfiguration {
        return MagicTextConfiguration(changes: [type], on: [underline], locatedAt: locatedAt, caseSensitive: false)
    }
    
    private func createMutableString(style: NSUnderlineStyle, color: UIColor? = nil, ranges: [NSRange]) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: text)
        
        ranges.forEach {
            mutableString.addAttribute(.underlineStyle, value: style.rawValue, range: $0)
            if let color = color {
                mutableString.addAttribute(.underlineColor, value: color, range: $0)
            }
        }
        
        return mutableString
    }
}
