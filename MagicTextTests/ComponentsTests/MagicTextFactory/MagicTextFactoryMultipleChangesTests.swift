//
//  MagicTextFactoryMultipleChangesTests.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 25/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import XCTest
@testable import MagicText

class MagicTextFactoryMultipleChangesTests: XCTestCase {
    
    private let text = "Let's add some magic to this text memento somalic"
    private let highlightText = ["et's add so"]
    private let highlightRange = [NSRange(location: 1, length: 11)]
    private lazy var textFactory = MagicTextFactory(text: text, font: font)
    
    func testItalicBoldSizeChange() {
        let config = MagicTextConfiguration(changes: [.bold, .italic, .size(increment: 10)], on: highlightText)
        let mutableString = textFactory.apply(configuration: config)
        
        let stringBold = createExpectedString(changes: [.traitBold], sizeChange: 10)
        let stringItalic = createExpectedString(changes: [.traitItalic], sizeChange: 10)
        let stringWrongSize = createExpectedString(changes: [.traitItalic, .traitBold], sizeChange: 1)
        let correctString = createExpectedString(changes: [.traitItalic, .traitBold], sizeChange: 10)
        XCTAssertNotEqual(mutableString, stringBold)
        XCTAssertNotEqual(mutableString, stringItalic)
        XCTAssertNotEqual(mutableString, stringWrongSize)
        XCTAssertEqual(mutableString, correctString)
    }
    
    func testBoldUnderlineSizeChange() {
        let config = MagicTextConfiguration(changes: [.bold, .underline(style: .single, color: UIColor.blue), .size(increment: 10)], on: highlightText)
        let mutableString = textFactory.apply(configuration: config)
        
        let stringBold = createExpectedString(changes: [.traitBold], sizeChange: 10)
        let stringUnderline = createUnderlinedString(style: .single, color: UIColor.blue)
        let stringWrongSize = createExpectedString(changes: [.traitItalic, .traitBold], sizeChange: 1)
        XCTAssertNotEqual(mutableString, stringBold)
        XCTAssertNotEqual(mutableString, stringUnderline)
        XCTAssertNotEqual(mutableString, stringWrongSize)
        
        let correctString = createUnderlinedString(mutableString: stringBold, style: .single, color: UIColor.blue)
        XCTAssertEqual(mutableString, correctString)
    }
    
    // MARK: - Private helpers
    private var font: UIFont {
        return UIFont.systemFont(ofSize: 0)
    }
    
    private func createExpectedString(changes: [UIFontDescriptor.SymbolicTraits], sizeChange: CGFloat) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: text)
        
        highlightRange.forEach {
            mutableString.addAttribute(.font, value: font.applyAttributes(changes, sizeChange: sizeChange), range: $0)
        }
        
        return mutableString
    }
    
    private func createUnderlinedString(mutableString: NSMutableAttributedString? = nil, style: NSUnderlineStyle, color: UIColor? = nil) -> NSMutableAttributedString {
        let mutableString = mutableString ?? NSMutableAttributedString(string: text)
        
        highlightRange.forEach {
            mutableString.addAttribute(.underlineStyle, value: style.rawValue, range: $0)
            if let color = color {
                mutableString.addAttribute(.underlineColor, value: color, range: $0)
            }
        }
        
        return mutableString
    }
}
