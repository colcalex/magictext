//
//  MagicTextFactoryTests.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 18/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import XCTest
@testable import MagicText

class MagicTextFactoryBoldTests: XCTestCase {
    
    private let text = "Let's add some magic to this text memento somalic"
    private lazy var textFactory = MagicTextFactory(text: text, font: font)
    
    func testBoldAnywhere() {
        let configuration = createConfig(makeBold: "et's add so", locatedAt: .anywhere)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(with: [NSRange(location: 1, length: 11)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testBoldStart() {
        let configuration = createConfig(makeBold: "som", locatedAt: .start)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(with: [NSRange(location: 10, length: 3), NSRange(location: 42, length: 3)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testBoldEnd() {
        let configuration = createConfig(makeBold: "to", locatedAt: .end)
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createMutableString(with: [NSRange(location: 21, length: 2), NSRange(location: 39, length: 2)])
        XCTAssertEqual(mutableString, expectedString)
    }
    
    // MARK: - Private helpers
    private var font: UIFont {
        return UIFont.systemFont(ofSize: 0)
    }
    
    private func createConfig(makeBold: String, locatedAt: MagicText.LocationMatchType) -> MagicTextConfiguration {
        return MagicTextConfiguration(changes: [.bold], on: [makeBold], locatedAt: locatedAt, caseSensitive: false)
    }
    
    private func createMutableString(with ranges: [NSRange]) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: text)
        
        ranges.forEach {
            mutableString.addAttribute(.font, value: font.applyAttributes([.traitBold]), range: $0)
        }
        
        return mutableString
    }
}
