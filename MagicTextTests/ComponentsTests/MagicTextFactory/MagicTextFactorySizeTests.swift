//
//  MagicTextFactorySizeTests.swift
//  MagicTextTests
//
//  Created by Alex Ioja-Yang on 18/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import XCTest
@testable import MagicText

class MagicTextFactorySizeTests: XCTestCase {
    
    private let text = "Let's add some magic to this text memento somalic"
    private let highlightText = ["et's add so"]
    private let highlightRange = [NSRange(location: 1, length: 11)]
    private lazy var textFactory = MagicTextFactory(text: text, font: font)
    
    func testSizeBold() {
        let configuration = createConfig(changes: [.bold, .size(increment: 5)])
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createBoldMutableString(with: highlightRange)
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testSizeItalic() {
        let configuration = createConfig(changes: [.size(increment: 5), .italic])
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createItalicMutableString(with: highlightRange)
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testSizeIncrease() {
        let configuration = createConfig(changes: [.size(increment: 10)])
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createSizeIncreaseString(increase: 10, with:highlightRange)
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testSizeDecrease() {
        let configuration = createConfig(changes: [.size(increment: -10)])
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = createSizeIncreaseString(increase: -10, with:highlightRange)
        XCTAssertEqual(mutableString, expectedString)
    }
    
    func testSizeUnchanged() {
        let configuration = createConfig(changes: [.size(increment: 0)])
        let mutableString = textFactory.apply(configuration: configuration)
        
        let expectedString = NSMutableAttributedString(string: text)
        XCTAssertEqual(mutableString, expectedString)
    }
    
    // MARK: - Private helpers
    private var font: UIFont {
        return UIFont.systemFont(ofSize: 0)
    }
    
    private func createConfig(changes: [MagicText.FontChangeType]) -> MagicTextConfiguration {
        return MagicTextConfiguration(changes: changes, on: highlightText)
    }
    
    private func createBoldMutableString(with ranges: [NSRange]) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: text)
        
        ranges.forEach {
            mutableString.addAttribute(.font, value: font.applyAttributes([.traitBold], sizeChange: 5), range: $0)
        }
        
        return mutableString
    }
    
    private func createItalicMutableString(with ranges: [NSRange]) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: text)
        
        ranges.forEach {
            mutableString.addAttribute(.font, value: font.applyAttributes([.traitItalic], sizeChange: 5), range: $0)
        }
        
        return mutableString
    }
    
    private func createSizeIncreaseString(increase: CGFloat, with ranges: [NSRange]) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: text)
        
        ranges.forEach {
            mutableString.addAttribute(.font, value: font.applyAttributes([], sizeChange: increase), range: $0)
        }
        
        return mutableString
    }
}
