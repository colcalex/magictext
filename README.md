

## Contents

- [About](#about)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## About

Use text highlighting in your app the easy way. MagicText aims at seamless integration with your project to allow your app effortless dynamic text highlighting.

## Installation

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate MagicText into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '9.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'MagicText'
end
```

Then, run the following command:

```bash
$ pod install
```

## Usage

### Quick Start

```swift
import MagicText

@IBOutlet private weak var label: UILabel!

func highlightText() {
	label.text = "This must be magic!"
	label.highlight(types: [.bold, .italic], occurancesOf: ["m", "s"])
}
```

That's it, set the text to your label and call the MagicText highlight extension with the highlighting types to apply and the targets.

### Power

The intention is to offer powerful capabilities while keeping it's usage simple and clear. That's why we tried to offer only 2 main customisation options: 
- **FontChangeType** - will directly affect the font (currently supporting bold, italic and size change)
- **LocationMatchType** - will allow you to only highlight occurances at start, end or anywhere inside words. For more information on customising it see the next section.

### Customising word separators

By default  `,`, `.`, `;` and `:` are word separators which will affect the behaviour of `LocationMatchType` attribute. They can be overriden by adding an entry in your projects **Info.plist**, property name **MagicSeparators** with a *String* value that contains all your word separators.

## Credits

- Alex Ioja-Yang

## License

MagicText is released under the MIT license. See LICENSE for details.