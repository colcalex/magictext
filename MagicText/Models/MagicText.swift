//
//  MagicText.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 15/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import UIKit

/// Container for all the Magic Properties required to be used
public struct MagicText {
    
    /// Where in words should the highlighting be applied
    public enum LocationMatchType {
        case start
        case end
        case anywhere
    }

    /// What highlighting effect should be applied
    public enum FontChangeType {
        case size(increment: CGFloat)
        case bold
        case italic
        case underline(style: NSUnderlineStyle, color: UIColor?)
    }

}
