//
//  MagicTextRangeGenerator.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 17/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import Foundation

internal struct MagicTextRangeGenerator {
    
    private let separators: String
    
    internal init(separators: String) {
        self.separators = separators
    }
    
    internal func getRanges(in text: String, of toRecognise: [String], occuringIn: MagicText.LocationMatchType, isCaseSensitive: Bool) -> [NSRange] {
        
        var ranges = [NSRange]()
        
        toRecognise.forEach {
            let target = isCaseSensitive ? $0 : $0.lowercased()
            let text = isCaseSensitive ? text : text.lowercased()
            ranges += findPosition(of: target, in: text, location: occuringIn)
        }
        
        return ranges
    }
    
    private func findPosition(of target: String, in text: String, location: MagicText.LocationMatchType) -> [NSRange] {
        let components = text.components(separatedBy: target)
        guard components.count > 1 else {
            return []
        }
        
        let separatorLenght = target.count
        var results = [NSRange]()
        for i in 1..<components.count {
            // Based on location restrictions we provide an early continue if occurance not valid for location
            if location == .start && !isStart(lastElement: components[i-1]) {
                continue
            } else if i < components.count &&
                location == .end &&
                !isEnd(nextElement: components[i]) {
                continue
            }
            
            var startIndex = Array(components[0..<i]).reduce(0, { result, word -> Int in
                return result + word.count
            })
            let previousOccurancesLenght = separatorLenght * (i-1)
            startIndex += previousOccurancesLenght
            results.append(NSRange(location: startIndex, length: separatorLenght))
        }
        return results
    }
    
    private func isStart(lastElement: String) -> Bool {
        guard let previousCharacter = lastElement.last else {
            return true
        }
        return separators.contains(previousCharacter)
    }
    
    private func isEnd(nextElement: String) -> Bool {
        guard let nextCharacter = nextElement.first else {
            return true
        }
        return separators.contains(nextCharacter)
    }
    
}
