//
//  MagicTextConfiguration.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 15/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import Foundation

/// Setup for the Magic
public struct MagicTextConfiguration {
    let isCaseSensitive: Bool
    let location: MagicText.LocationMatchType
    let changeTypes: [MagicText.FontChangeType]
    let targetWords: [String]
    
    public init(changes: [MagicText.FontChangeType], on words: [String], locatedAt location: MagicText.LocationMatchType = .anywhere, caseSensitive: Bool = false) {
        self.isCaseSensitive = caseSensitive
        self.location = location
        self.changeTypes = changes
        self.targetWords = words
    }
}
