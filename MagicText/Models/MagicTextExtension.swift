//
//  MagicTextExtension.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 24/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import Foundation

typealias Attribute = (key: NSAttributedString.Key, value: Any)

extension MagicText {
    
    static func transform(changes: [FontChangeType], on font: UIFont) -> [Attribute] {
        var attributes: [Attribute] = []
        if let fontChange = fontChanges(from: changes, for: font) {
            attributes.append(fontChange)
        }
        if let underlineChanges = fontUnderline(from: changes, for: font) {
            attributes.append(contentsOf: underlineChanges)
        }
        return attributes
    }
    
    // MARK: - Underline effects => NSAttributedString.Key = .underlineStyle
    static private func fontUnderline(from changes: [FontChangeType], for font: UIFont) -> [Attribute]? {
        let underlinesArray = changes.filter {
            if case FontChangeType.underline(_) = $0 {
                return true
            }
            return false
        }
        if underlinesArray.count > 1 {
            print("\n    !!!!    MAGIC TEXT    !!!!\nMultiple underline effects were provided, please note only the first one will be applied")
        }
        if let underlineStyle = underlinesArray.first,
            case FontChangeType.underline(let style, let color) = underlineStyle {
            var attributes = [Attribute(key: NSAttributedString.Key.underlineStyle, value: style.rawValue)]
            if let underlineColor = color {
                attributes.append(Attribute(key: .underlineColor, value: underlineColor))
            }
            return attributes
        }
        return nil
    }
    
    // MARK: - Font effects => NSAttributedString.Key = .font
    static private func fontChanges(from changes: [FontChangeType], for font: UIFont) -> Attribute? {
        let fontChanges = separateFontChanges(from: changes)
        let fontSizeChange = separateFontSize(from: changes)
        if fontChanges.count == 0 && fontSizeChange == 0 {
            return nil
        }
        let newFont = font.applyAttributes(fontChanges, sizeChange: fontSizeChange)
        return Attribute(key: .font, value: newFont)
    }
    
    static private func separateFontChanges(from changes: [FontChangeType]) -> [UIFontDescriptor.SymbolicTraits] {
        return changes.compactMap {
            switch $0 {
            case .bold:
                return UIFontDescriptor.SymbolicTraits.traitBold
            case .italic:
                return UIFontDescriptor.SymbolicTraits.traitItalic
            default:
                return nil
            }
        }
    }
    
    static private func separateFontSize(from changes: [FontChangeType]) -> CGFloat {
        let sizeChange = changes.compactMap { change -> CGFloat? in
            if case FontChangeType.size(let increment) = change {
                return increment
            }
            return nil
        }
        if sizeChange.count > 1 {
            print("\n    !!!!    MAGIC TEXT    !!!!\nMultiple size changes were provided, please note only the first one will be applied")
        }
        return sizeChange.first ?? 0
    }
}
