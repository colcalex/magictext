//
//  MagicTextFactory.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 15/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import UIKit

/// Create the Magic
public struct MagicTextFactory {

    private var separators: String {
        return PlistSeparatorReader.getSeparators()
    }
    
    private let text: String
    private let font: UIFont
    
    public init(text: String, font: UIFont) {
        self.text = text
        self.font = font
    }
    
    /// Returns a NSMutableAttributedString to be set as attributedText to the desired IBOutlet
    public func apply(configuration: MagicTextConfiguration) -> NSMutableAttributedString {
        let highlightRanges = MagicTextRangeGenerator(separators: separators).getRanges(in: text, of: configuration.targetWords, occuringIn: configuration.location, isCaseSensitive: configuration.isCaseSensitive)
        
        let mutableString = NSMutableAttributedString(string: text)
        let attributes = MagicText.transform(changes: configuration.changeTypes, on: font)
        
        attributes.forEach { attribute in
            highlightRanges.forEach { range in
                mutableString.addAttribute(attribute.key, value: attribute.value, range: range)
            }
        }
        
        return mutableString
    }
    
}
