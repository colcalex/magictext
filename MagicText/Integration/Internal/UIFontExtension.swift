//
//  UIFontExtension.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 18/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import UIKit

extension UIFont {
    
    internal func applyAttributes(_ attributes: [UIFontDescriptor.SymbolicTraits], sizeChange: CGFloat = 0) -> UIFont {
        let value = attributes.map({ return $0.rawValue }).reduce(0, +)
        return withTraits(traits: UIFontDescriptor.SymbolicTraits(rawValue: value), size: pointSize + sizeChange)
    }
    
    private func withTraits(traits: UIFontDescriptor.SymbolicTraits, size: CGFloat) -> UIFont {
        guard let descriptor = fontDescriptor.withSymbolicTraits(traits) else {
            print("\n\n!!!!!     !!!!!     MagicText failed to convert font descriptor. Please see the repo https://gitlab.com/colcalex/magictext and contact to inform. Thank you for your help \n\n")
            return self
        }
        return UIFont(descriptor: descriptor, size: size)
    }
}
