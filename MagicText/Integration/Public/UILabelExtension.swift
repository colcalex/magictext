//
//  UIViewExtensions.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 22/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import UIKit

extension UILabel {
    
    private var factory: MagicTextFactory {
        let text: String = self.text ?? ""
        return MagicTextFactory(text: text, font: self.font)
    }
    
    public func highlight(types: [MagicText.FontChangeType], occurancesOf words: [String]) {
        makeMagic(changes: types, occurancesOf: words)
    }
    
    public func highlight(types: [MagicText.FontChangeType], occurancesOf words: [String], location: MagicText.LocationMatchType) {
        makeMagic(changes: types, occurancesOf: words, locatedAt: location)
    }
    
    public func highlight(types: [MagicText.FontChangeType], occurancesOf: [String], location: MagicText.LocationMatchType, caseSensitive: Bool) {
        makeMagic(changes: types, occurancesOf: occurancesOf, locatedAt: location, isCaseSensitive: caseSensitive)
    }
    
    private func makeMagic(changes: [MagicText.FontChangeType], occurancesOf targets: [String], locatedAt location: MagicText.LocationMatchType = .anywhere, isCaseSensitive: Bool = false) {
        attributedText = factory.apply(configuration: MagicTextConfiguration(changes: changes, on: targets, locatedAt: location, caseSensitive: isCaseSensitive))
    }

}
