//
//  PlistSeparatorReader.swift
//  MagicText
//
//  Created by Alex Ioja-Yang on 24/11/2018.
//  Copyright © 2018 Alex Ioja-Yang. All rights reserved.
//

import Foundation

internal final class PlistSeparatorReader {
    
    static private var defaultSeparators = " ,.;:"
    
    static internal func getSeparators() -> String {
        guard let string = Bundle.main.object(forInfoDictionaryKey: "MagicSeparators") as? String else {
            return defaultSeparators
        }
        return string
    }
}
