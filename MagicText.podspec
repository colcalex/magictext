Pod::Spec.new do |s|


s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "MagicText"
s.summary = "Highlight text in your app the quick and easy way."
s.version = "0.1.9"
s.license = { :type => "MIT", :file => "LICENSE" }

s.author = { "Alex Ioja-Yang" => "contact@way2t.co.uk" }
s.homepage = "https://gitlab.com/colcalex/magictext"
s.source = { :git => "https://gitlab.com/colcalex/magictext", 
             :tag => "#{s.version}" }
s.source_files = "MagicText/**/*.{swift}"
s.swift_version = "4.2"

end
